# DEPRECATED for sites created after 6/2020 #

# README #


This modules installs configuration for some paragraph types and some preprocess logic.

paragraph types:
-billboard
-impact_banner
-mosaic
-signpost
-signpost_container

It should be enabled along with ma_config_sprout but can be used without it as well.